Annotate non-coding RNAs and repetitive elements. These include rRNAs, tRNAs, miRNAs, repetitive elements, and other miscelaneous.
