/*
========================================================================================
 Annotate ncRNAs with sRNA-seq data. Started October 2018.

 #### Authors
 Pablo González <pablo.gonzalez@cinvestav.mx>

----------------------------------------------------------------------------------------
*/

/*
 * SET UP CONFIGURATION VARIABLES
 */

// Pipeline version
version = 0.03

// Configurable variables
params.project = false
params.genome = "Celegans.fa"
params.gtf = "Celegans.gtf"
params.adapterSeq = "ATCG"
params.tabuSeq = "CTGA"
params.phred = "15/4"
params.dust = 20
params.minLen = 17
params.mismatches = 1
params.ranmax = 50
params.bowtieM = params.ranmax
params.dicermin = 18
params.dicermax = 24
params.mincov = 5
params.pad = 10
params.miRNAs = "mature.fa"
params.hairpins = "high_conf_insect_hairpin.fa"
params.dustSrc = "custom_dust_releases.tar.bz2"
params.queryRepSrc = "RepeatMasker/util/queryRepeatDatabase.pl"
params.rfam = "data/dbs/Rfam.cm"
params.rfamFams = "data/dbs/family.txt.gz"
params.chunkSize = 1000
params.reads = "data/*{1,2}.fastq.gz"
params.saveReference = 'True'
params.outdir = './results'


/*
 * Execution example:
 cd /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/
 nohup nextflow -C /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/nf_mazorka.conf run /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/main.nf -resume -with-timeline --project t1 --outdir /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/ --reads '/LUSTRE/usuario/pgonzale/ncrna-nf/data/raw/*.fastq.gz' --genome /LUSTRE/usuario/pgonzale/ncrna-nf/data/genome/test2.fa --gtf /LUSTRE/usuario/pgonzale/ncrna-nf/data/genome/test.gtf --adapterSeq "TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC" --tabuSeq "GTTCAGAGTTCTACAGTCCGACGATC" --miRNAs "/LUSTRE/usuario/pgonzale/masterMon/data/miRNAs/RHC_mature.fa" --hairpins "/LUSTRE/usuario/pgonzale/masterMon/data/miRNAs/high_conf_insect_hairpin.fa" --minLen 17 --chunkSize 1 --dustSrc "/LUSTRE/usuario/pgonzale/masterMon/output/ncRNA/MapMi/HelperPrograms/custom_dust_releases.tar.bz2" --queryRepSrc "/LUSTRE/usuario/pgonzale/software/miniconda2/envs/py2.7/share/RepeatMasker/util/queryRepeatDatabase.pl" --rfam /LUSTRE/usuario/pgonzale/ncrna-nf/data/dbs/Rfam.cm --rfamFams "data/dbs/family.txt.gz" > /LUSTRE/usuario/pgonzale/ncrna-nf/logs/nft1$(date +"%y_%F_%T").log 2>&1 &
 */


// Validate inputs

if ( params.genome ){
    Channel
        .fromPath(params.genome)
        .ifEmpty {  exit 1, "genome file not found: ${params.genome}" }
        .into { genome_bowtieIndex ; genome_mirdeep2 ; genome_sBowtie ; genome_trnascan ; genome_rnammer ; genome_repMod ; genome_repMask ; genome_MapMi ; genome_sstack ; genome_annot}
		
}

if ( params.gtf ){
    Channel
        .fromPath(params.gtf)
        .ifEmpty {  exit 1, "GTF file not found: ${params.gtf}" }
        .set { annot_gtf}
		
}


if ( params.miRNAs ){
    Channel
        .fromPath(params.miRNAs)
        .ifEmpty {  exit 1, "miRNAs' file not found: ${params.miRNAs}" }
        .into { miRNA_MapMi ; miRNA_mirdeep2 }
}

if ( params.hairpins ){
    Channel
        .fromPath(params.hairpins)
        .ifEmpty {  exit 1, "hairpins' file not found: ${params.hairpins}" }
        .set { hairpins_mirdeep2 }
}

if ( params.rfam ){
    Channel
        .fromPath(params. rfam)
        .ifEmpty {  exit 1, " rfam file not found: ${params.rfam}" }
        .set {  rfam_cmsearch }
}

if ( params.rfamFams ){
    Channel
        .fromPath(params.rfamFams)
        .ifEmpty {  exit 1, "Rfam families file not found: ${params.rfamFams}" }
        .set {  rfam_fam }
}

if ( params.dustSrc ){
    Channel
        .fromPath(params.dustSrc)
        .ifEmpty {  exit 1, "Custom dust file not found: ${params.dustSrc}" }
        .set { dustSrc }
}

if ( params.queryRepSrc ){
    Channel
        .fromPath(params.queryRepSrc)
        .ifEmpty {  exit 1, "queryRepeatDatabase.pl script not found: ${params.queryRepSrc}" }
        .set { queryRepSrc }
}

if ( params.minLen < 17 ){
	println "miRDeep2 requires minimal length to be at least 17."
	exit 1
}


/*
 * Create a channel for input read files
 */
Channel
    .fromFilePairs( params.reads, size: 1 )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}" }
    .set { reads_reaper  }


if( workflow.profile == 'standard' && !params.project ) exit 1, "No project ID found! Use --project"

// Header log info
log.info "========================================="
log.info "ncRNA-nf v${version}"
log.info "========================================="
log.info "Reads             : ${params.reads}"
log.info "Genome            : ${params.genome}"
log.info "miRNAs            : ${params.miRNAs}"
log.info "hairpins          : ${params.hairpins}"
log.info "3' adapter        : ${params.adapterSeq}"
log.info "Tabu              : ${params.tabuSeq}"
log.info "Quality threshold : ${params.phred}"
log.info "DUST score        : ${params.dust}"
log.info "Min length        : ${params.minLen}"
log.info "Mismatches        : ${params.mismatches}"
log.info "Ranmax            : ${params.ranmax}"
log.info "BowtieM           : ${params.bowtieM}"
log.info "Dicer min.        : ${params.dicermin}"
log.info "Dicer max.        : ${params.dicermax}"
log.info "Min. coverage     : ${params.mincov}"
log.info "Pad               : ${params.pad}"
log.info "Dust code         : ${params.dustSrc}"
log.info "QueryRepeats.pl   : ${params.queryRepSrc}"
log.info "Rfam models       : ${params.rfam}"
log.info "Rfam families     : ${params.rfamFams}"
log.info "Current home      : $HOME"
log.info "Current user      : $USER"
log.info "Current path      : $PWD"
log.info "Script dir        : $baseDir"
log.info "Working dir       : $workDir"
log.info "Output dir        : ${params.outdir}"
log.info "========================================="


Channel
    .fromPath(params.genome)
    .splitFasta(by: params.chunkSize)
    .into { trnascan_fasta ; rfam_fasta }



/*
 * STEP 1.1.1 - Rfam
 */
process rfam {
  publishDir "${params.outdir}/Rfam", mode: 'copy'

  input:
  file rfam from rfam_cmsearch.collect()
  file fasta from rfam_fasta

  output:
  file "genome.rfam.gz" into rfam_tmp_tbl
  file "temp.gff3.gz" into rfam_tmp_gff3

  script:
  """
  cmpress $rfam
  cmsearch --rfam --cpu ${task.cpus} --tblout genome.rfam $rfam $fasta
  awk 'NR>2{ if(\$10 == "-"){start=\$9+1; end=\$8+1} else {start=\$8; end=\$9}; printf "%s\\tinfernal\\t%s\\t%d\\t%d\\t%7.2f\\t%s\\t.\\tRfamID=%s\\n" ,\$1,\$3,start,end,\$15,\$10,\$4}' genome.rfam | gzip > temp.gff3.gz
  gzip genome.rfam
  """
}

rfam_tmp_tbl.collectFile(name:'rfam.tab.gz', newLine: true)
    .set { rfam_tbl }

rfam_tmp_gff3.collectFile(name:'infernal.gff3', newLine: true)
    .set { rfam_gff3 }

 /*
  * STEP 1.2.1 - tRNAscan
 */
process trnascan {
    input:
    file fasta from trnascan_fasta

    output:
    file "trnascan.out" into trnascan_frag

    script:
    """
    tRNAscan-SE -Q -o trnascan.out $fasta
    """
}

trnascan_frag.collectFile(name:'tRNAs.tab', newLine: true)
    .set { trnascan_awk }
    
/*
 * STEP 1.2.2 - tRNAscan
 */

process tRNA_awk {
   publishDir "${params.outdir}/tRNAscan", mode: 'copy'

   input:
   file tRNAs from trnascan_awk

   output:
   file "tRNAs.gtf.gz" into tRNAscan_gtf

   shell:
   """
   awk '\$3 ~ /^[0-9]+\$/ { if(\$3 > \$4){strand = "-"; start=\$4+1; end=\$3+1} else {strand = "+"; start=\$3; end=\$4}; printf "%s\\ttRNAscan-SE\\ttRNA\\t%d\\t%d\\t%7.2f\\t%s\\t.\\taa=%s\\n" ,\$1,start,end,\$9,strand,\$5}' tRNAs.tab | gzip > tRNAs.gtf.gz
   """
}

/*
 * STEP 1.3 - RNAmmer
 */
process rnammer {
   publishDir "${params.outdir}/RNAmmer", mode: 'copy'

   input:
   file fasta from genome_rnammer

   output:
   file "rRNAs.gff" into rnammer_out

   script:
   """
   rnammer -S euk -m tsu,lsu,ssu -gff rRNAs.gff < $fasta
   """
}

/*
 * STEP 1.4 - MapMi
 */

process mapmi {
   publishDir "${params.outdir}/MapMi", mode: 'copy'

   input:
   file miRNAs from miRNA_MapMi
   file fasta from genome_MapMi
   file cdust from dustSrc

   output:
   file "MapmiOut.tab" into mapmiOut_tab
   file "mapmi.gff3.gz" into mapmiOut_gff3

   shell:
   """
   mkdir -p HelperPrograms/
   mkdir -p RawData/Others/
   seqret -sequence $fasta -outseq RawData/Others/test.fa
   tar jxf $cdust -C HelperPrograms/
   make -C HelperPrograms/Custom_dust_releases/dust2/
   ln -s \$PWD/HelperPrograms/Custom_dust_releases/dust2/dust2 .
   ln -s /data/software/bowtie-1.2.2-linux-x86_64/bowtie-build HelperPrograms/
   ln -s /data/software/bowtie-1.2.2-linux-x86_64/bowtie HelperPrograms/

   MapMi-MainPipeline-v159-b32.pl --queryFile $miRNAs --min_score 0 --min_len $params.minLen --bowtie_m $params.bowtieM --outputPrefix MapmiOut
   awk 'BEGIN { RS = ">"; ORS = "\\n"; FS = "\\n"; OFS = "\\t" } ; { print \$1, \$2}' MapmiOut.dust | tail -n +2 > MapmiOut.tab
   awk '{ printf "%s\\tMapMi\\tmiRNA\\t%d\\t%d\\t%6.2f\\t%s\\t.\\tmiRNA=%s;mismatches=%d\\n" ,\$6,\$11,\$12,\$16,\$7,\$1,\$5}' MapmiOut.tab | gzip > mapmi.gff3.gz
   """
}

/*
 * STEP 1.5 - RepeatModeler
 */

process repeatmodeler {

   input:
   file fasta from genome_repMod

   output:
   file("genome-families.fa") into modRepts_fa
   file("genome-families.stk") into modRepts_stk
   

   script:
   """
   BuildDatabase -name genome -engine ncbi $fasta
   RepeatModeler -engine ncbi -pa ${task.cpus} -database genome -srand 2017
   """
}


/*
 * STEP 1.5 - RepeatMasker
 */

process repeatmasker {
   publishDir "${params.outdir}/RepeatMasker", mode: 'copy'

   input:
   file fasta from genome_repMask
   file reps from modRepts_fa
   file query from queryRepSrc

   output:
   file("*.gff") into RM_gff
   file("*.out") into RM_out
   file("*.tbl") into RM_tbl
   file("all.repeats") into repsFa
   

   shell:
   """
   #! /bin/bash
   source activate ncrna
   cat $reps <(./queryRepeatDatabase.pl -species nematoda) > all.repeats
   RepeatMasker -gff  -lib all.repeats -pa ${task.cpus} $fasta
   """
}

/*
 * STEP 1.6 - Reaper
 */

process reaper {
    tag "$name"
    publishDir "${params.outdir}/Reaper", mode: 'copy'

    input:
    set val(name), file(reads) from reads_reaper

    output:
    set val(name), file("${name}.trim.fq.gz") into trimd_fastqc, trimd_tally, trimd_mirdeep2, trimd_sBowtie, trimd_dCompress

    shell:
    """
    reaper -clean-length $params.minLen -3pa $params.adapterSeq -tabu $params.tabuSeq -mr-tabu 16/2/1/0 -3p-global 12/1/0/0 -3p-prefix 8/1/0 -3p-head-to-tail 0  -dust-suffix $params.dust -geom no-bc -nnn-check 0/0 -qqq-check $params.phred -i $reads -basename ${name}
    mv ${name}.lane.clean.gz ${name}.trim.fq.gz
    """
}

/*
 * STEP 2.1 - FastQC
 */
process fastqc {
    tag "$name"
    publishDir "${params.outdir}/fastqc", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_fastqc

    output:
    file "*_fastqc.{zip,html}" into fastqc_results

    shell:
    """
    fastqc $reads -t ${task.cpus}
    """
}

/*
 * STEP 2.2 - dCompress
 */
process dcompress {
    tag "$name"
    publishDir "${params.outdir}/dCompress", mode: 'copy'

    input:
	set val(name), file(reads) from trimd_dCompress

    output:
    file "${name}.trim.fq" into dcompressed

    shell:
    """
    zcat $reads > ${name}.trim.fq
    """
}

/*
 * STEP N - Tally
process fastqc {
    tag "$name"
    publishDir "${params.outdir}/Tally", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_tally
    
    output:
    file "${reads.baseName - '.fa.gz'}.tally.fastq.gz" into tallied_sBowtie

    shell:
    """
    tally -i $reads -o ${reads.baseName - '.fastq.gz'}.tally.fastq.gz -format '>trn_%I_%C%n%R%n%Q%n'
    """
}
 */

 
/*
 * STEP 3.1 - miRDeep2
 */
process mirdeep2 {
    publishDir "${params.outdir}/miRDeep2", mode: 'copy'

    input:
    file reads from dcompressed.collect()
  	file fasta from genome_mirdeep2
  	file miRNAs from miRNA_mirdeep2
  	file hairpin from hairpins_mirdeep2

    output:
    file "miRDeep2.{bed,txt}" into miRDeep_out
    file "miRDeep2_report.log" into miRDeep_log

    script:
    prefix = fasta.toString() - ~/(\.fa)?(\.fasta)?(\.gz)?$/
    """
  	list=`echo {a..c}`
  	for c1 in \$list
  	do
  		for c2 in \$list
  		do  
  			for c3 in \$list
  			do  
  				echo \$c1\$c2\$c3 >> let3Comb.txt
  			done
  		done
  	done
  
  	find . -name "*.fq" > temp
  	head -n \$(wc -l  temp | cut -f1 -d" ") let3Comb.txt  > temp2
  	paste -d"\\t" temp temp2 > expDesign.txt
  	bowtie-build $fasta $prefix
    mapper.pl expDesign.txt -d -e -h -j -l $params.minLen -m -p $prefix -s configAll.fa -t configAll.arf -v
  	miRDeep2.pl configAll.fa $fasta configAll.arf $miRNAs none $hairpin 2> miRDeep2_report.log
  	cat result*.csv | grep -P 'provisional id|yes\\t|no\\t' > miRDeep2.txt
  	cp result*.bed miRDeep2.bed
    """
}
/*
 * STEP 3.2 - ssBowtie
 */
process ssbowtie {
    tag "$name"
    publishDir "${params.outdir}/ShortStack/ssBowtie", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_sBowtie
    file fasta from genome_sBowtie.collect()

    output:
    file "${name}.trim.bam" into bam_smerge

    shell:
    """
    ShortStack --bowtie_cores ${task.cpus} --mismatches $params.mismatches \
    --mmap u --ranmax $params.ranmax --bowtie_m $params.bowtieM \
    --readfile $reads --genomefile $fasta \
    --outdir ${name} --align_only
	mv ${name}/${name}.trim.bam ${name}.trim.bam
    """
}

/*
 * STEP 4.1 - ssMergeBam
 */
process ssmerge {
    publishDir "${params.outdir}/ShortStack/sMerge", mode: 'copy'

    input:
    file reads from bam_smerge.collect()

    output:
    file "merged.bam" into bam_sstack
	file "merged.bam.bai" into bai_sstack

    shell:
    """
    samtools merge -f -@ ${task.cpus} merged.bam $reads
    samtools index -@ ${task.cpus} merged.bam
    """
}

/*
 * STEP 5.1 - ShortStack
 */
process shortstack {
    publishDir "${params.outdir}/ShortStack/main", mode: 'copy'

    input:
	file bam from bam_sstack
    file idx from bai_sstack
	file fasta from genome_sstack

    output:
    file "ShortStack*" into sstack_gff
	file "Results.txt" into sstack_txt
	file "Log.txt" into sstack_log

    shell:
    """
    ShortStack  --genomefile $fasta --dicermin $params.dicermin --dicermax $params.dicermax --mincov $params.mincov --pad $params.pad --bamfile $bam --outdir tmp
	mv tmp/* .
    """
}

/*
 * STEP 6.1 - Reduce annotation
 */
process annotreduce {
    publishDir "${params.outdir}/annotReduce", mode: 'copy'

    input:
	file RM_gff
	file RM_out
	file rf_gff3 from rfam_gff3
	file fasta from genome_annot
	file gtf from annot_gtf
	file mampi from mapmiOut_gff3
	file tRNAs from tRNAscan_gtf
	file rna_family from rfam_fam
	file rRNAs from rnammer_out
	file mirdeep2 from miRDeep_out

    output:
    file "named_repeats*" into sc0
    file "intron_exon_GR.Rds" into sc1
    file "ncRNAs*" into sc2
    file "segmentation_annotation*" into sc3

    script: // This script is bundled with the pipeline, in ncrna-nf/scripts/
    """
	Rscript ${baseDir}/00_get_named_repeats_GRanges.R $fasta $RM_gff $RM_out
	Rscript ${baseDir}/01_get_intron_exon_GRanges.R $fasta $gtf
	Rscript ${baseDir}/02_get_ncRNA_GRanges.R $rna_family $fasta $rf_gff3 $rRNAs $tRNAs $mampi $mirdeep2
	Rscript ${baseDir}/03_get_genome_segmentation_annotation_GRanges.R
    """
}