/*
========================================================================================
 Annotate ncRNAs with sRNA-seq data. Started October 2018.

 #### Authors
 Pablo González <pablo.gonzalez@cinvestav.mx>

----------------------------------------------------------------------------------------
*/

/*
 * SET UP CONFIGURATION VARIABLES
 */

// Pipeline version
version = 0.01

// Configurable variables
params.project = false
params.genome = "Celegans.fa"
params.adapterSeq = "ATCG"
params.tabuSeq = "CTGA"
params.phred = "15/4"
params.dust = 20
params.minLen = 17
params.mismatches = 1
params.ranmax = 50
params.bowtieM = params.ranmax
params.dicermin = 18
params.dicermax = 24
params.mincov = 5
params.pad = 10
params.miRNAs = "mature.fa"
params.hairpins = "high_conf_insect_hairpin.fa"
params.chunkSize = 1000
params.reads = "data/*{1,2}.fastq.gz"
params.saveReference = 'True'
params.outdir = './results'

/*
 * Execution example:
 cd /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/
nohup nextflow -C /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/nf_mazorka.conf run /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/minimal.nf -resume -with-timeline --project t1 --outdir /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/ --reads '/LUSTRE/usuario/pgonzale/ncrna-nf/data/raw/*.fastq.gz' --genome /LUSTRE/usuario/pgonzale/ncrna-nf/data/genome/c_elegans.PRJNA13758.WS260.genomic.fa --adapterSeq "TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC" --tabuSeq "GTTCAGAGTTCTACAGTCCGACGATC" --miRNAs "/LUSTRE/usuario/pgonzale/masterMon/data/miRNAs/RHC_mature.fa" --hairpins "/LUSTRE/usuario/pgonzale/masterMon/data/miRNAs/high_conf_insect_hairpin.fa" --minLen 17 > /LUSTRE/usuario/pgonzale/ncrna-nf/logs/nft1$(date +"%y_%F_%T").log 2>&1 &
 */


// Validate inputs

if ( params.genome ){
    Channel
        .fromPath(params.genome)
        .ifEmpty {  exit 1, "genome file not found: ${params.genome}" }
        .into { genome_bowtieIndex ; genome_mirdeep2 ; genome_sBowtie ; genome_sstack }
}

if ( params.miRNAs ){
    Channel
        .fromPath(params.miRNAs)
        .ifEmpty {  exit 1, "miRNAs' file not found: ${params.miRNAs}" }
        .into { miRNA_MapMi ; miRNA_mirdeep2 }
}

if ( params.hairpins ){
    Channel
        .fromPath(params.hairpins)
        .ifEmpty {  exit 1, "hairpins' file not found: ${params.hairpins}" }
        .into { hairpins_mirdeep2 }
}

/*
 * Create a channel for input read files
 */
Channel
    .fromFilePairs( params.reads, size: 1 )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}" }
    .into { reads_reaper  }


if( workflow.profile == 'standard' && !params.project ) exit 1, "No project ID found! Use --project"

// Header log info
log.info "========================================="
log.info "ncRNA-nf v${version}"
log.info "========================================="
log.info "Reads             : ${params.reads}"
log.info "Genome            : ${params.genome}"
log.info "miRNAs            : ${params.miRNAs}"
log.info "hairpins          : ${params.hairpins}"
log.info "3' adapter        : ${params.adapterSeq}"
log.info "Tabu              : ${params.tabuSeq}"
log.info "Quality threshold : ${params.phred}"
log.info "DUST score        : ${params.dust}"
log.info "Min length        : ${params.minLen}"
log.info "Mismatches        : ${params.mismatches}"
log.info "Ranmax            : ${params.ranmax}"
log.info "BowtieM           : ${params.bowtieM}"
log.info "Dicer min.        : ${params.dicermin}"
log.info "Dicer max.        : ${params.dicermax}"
log.info "Min. coverage     : ${params.mincov}"
log.info "Pad               : ${params.pad}"
log.info "Current home      : $HOME"
log.info "Current user      : $USER"
log.info "Current path      : $PWD"
log.info "Script dir        : $baseDir"
log.info "Working dir       : $workDir"
log.info "Output dir        : ${params.outdir}"
log.info "========================================="


Channel
    .fromPath(params.genome)
    .splitFasta(by: params.chunkSize)
    .set { trnascan_fasta }


/*
 * PREPROCESSING - Build Bowtie index
 */

process bowtieindex {
	tag "$fasta"
	publishDir path: "${params.outdir}/reference_genome", saveAs: { params.saveReference ? it : null }, mode: 'copy'

	input:
	file fasta from genome_bowtieIndex

	output:
	file '*.ebwt' into bindex_mirdeep2
	val(prefix) into bname_mirdeep2

	script:
	prefix = fasta.toString() - ~/(\.fa)?(\.fasta)?(\.gz)?$/
	"""
	bowtie-build $fasta $prefix
	"""
}

/*
 * STEP 1 - Reaper
 */

process reaper {
    tag "$name"
    publishDir "${params.outdir}/Reaper", mode: 'copy'

    input:
    set val(name), file(reads) from reads_reaper

    output:
    set val(name), file("${name}.trim.fq.gz") into trimd_fastqc, trimd_tally, trimd_mirdeep2, trimd_sBowtie, trimd_dCompress

    shell:
    """
    reaper -clean-length $params.minLen -3pa $params.adapterSeq -tabu $params.tabuSeq -mr-tabu 16/2/1/0 -3p-global 12/1/0/0 -3p-prefix 8/1/0 -3p-head-to-tail 0  -dust-suffix $params.dust -geom no-bc -nnn-check 0/0 -qqq-check $params.phred -i $reads -basename ${name}
    mv ${name}.lane.clean.gz ${name}.trim.fq.gz
    """
}

/*
 * STEP 2.1 - FastQC
 */
process fastqc {
    tag "$name"
    publishDir "${params.outdir}/fastqc", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_fastqc

    output:
    file "*_fastqc.{zip,html}" into fastqc_results

    shell:
    """
    fastqc $reads -t ${task.cpus}
    """
}

/*
 * STEP 2.2 - dCompress
 */
process dcompress {
    tag "$name"
    publishDir "${params.outdir}/dCompress", mode: 'copy'

    input:
	set val(name), file(reads) from trimd_dCompress

    output:
    file "${name}.trim.fq" into dcompressed

    shell:
    """
    zcat $reads > ${name}.trim.fq
    """
}

/*
 * STEP N - Tally
process fastqc {
    tag "$name"
    publishDir "${params.outdir}/Tally", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_tally
    
    output:
    file "${reads.baseName - '.fa.gz'}.tally.fastq.gz" into tallied_sBowtie

    shell:
    """
    tally -i $reads -o ${reads.baseName - '.fastq.gz'}.tally.fastq.gz -format '>trn_%I_%C%n%R%n%Q%n'
    """
}
 */

 
/*
 * STEP 3.1 - miRDeep2
 */
process mirdeep2 {
    publishDir "${params.outdir}/miRDeep2", mode: 'copy'

    input:
    file reads from dcompressed.collect()
	file idx from bindex_mirdeep2.collect()
	val(prefix) from bname_mirdeep2
	file fasta from genome_mirdeep2
	file miRNAs from miRNA_mirdeep2
	file hairpin from hairpins_mirdeep2

    output:
    file "miRDeep*" into miRDeep_out

    shell:
    """
	list=`echo {a..c}`
	for c1 in \$list
	do
		for c2 in \$list
		do  
			for c3 in \$list
			do  
				echo \$c1\$c2\$c3 >> let3Comb.txt
			done
		done
	done

	find . -name "*.fq" > temp
	head -n \$(wc -l  temp | cut -f1 -d" ") let3Comb.txt  > temp2
	paste -d"\\t" temp temp2 > expDesign.txt
	
    mapper.pl expDesign.txt -d -e -h -j -l $params.minLen -m -p $prefix -s configAll.fa -t configAll.arf -v
	miRDeep2.pl configAll.fa $fasta configAll.arf $miRNAs none $hairpin 2> miRDeep2_report.log
	cat result*.csv | grep -P 'provisional id|yes\\t|no\\t' > miRDeep.txt
	cp result*.bed miRDeep.bed
    """
}
/*
 * STEP 3.2 - ssBowtie
 */
process ssbowtie {
    tag "$name"
    publishDir "${params.outdir}/ShortStack/ssBowtie", mode: 'copy'

    input:
    set val(name), file(reads) from trimd_sBowtie
    file fasta from genome_sBowtie.collect()

    output:
    file "${name}.trim.bam" into bam_smerge

    shell:
    """
    ShortStack --bowtie_cores ${task.cpus} --mismatches $params.mismatches \
    --mmap u --ranmax $params.ranmax --bowtie_m $params.bowtieM \
    --readfile $reads --genomefile $fasta \
    --outdir ${name} --align_only
	mv ${name}/${name}.trim.bam ${name}.trim.bam
    """
}

/*
 * STEP 4.1 - ssMergeBam
 */
process ssmerge {
    publishDir "${params.outdir}/ShortStack/sMerge", mode: 'copy'

    input:
    file reads from bam_smerge.collect()

    output:
    file "merged.bam" into bam_sstack
	file "merged.bam.bai" into bai_sstack

    shell:
    """
    samtools merge -f -@ ${task.cpus} merged.bam $reads
    samtools index -@ ${task.cpus} merged.bam
    """
}

/*
 * STEP 5.1 - ShortStack
 */
process shortstack {
    publishDir "${params.outdir}/ShortStack/main", mode: 'copy'

    input:
	file bam from bam_sstack
    file idx from bai_sstack
	file fasta from genome_sstack

    output:
    file "ShortStack*" into sstack_gff
	file "Results.txt" into sstack_txt
	file "Log.txt" into sstack_log

    shell:
    """
    ShortStack  --genomefile $fasta --dicermin $params.dicermin --dicermax $params.dicermax --mincov $params.mincov --pad $params.pad --bamfile $bam --outdir tmp
	mv tmp/* .
    """
}