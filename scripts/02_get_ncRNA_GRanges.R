## Get the ranges covered by ncRNAs, and their anti-sense version

## I still need to redo the "piRNA" predictions!

library(GenomicRanges)
library(rtracklayer)
library(Rsamtools)

# Rfam files
## Get the ranges covered by exons, introns, and their anti-sense versions
args = commandArgs(trailingOnly=TRUE)
if (length(args) < 8) {
  stop("Unexpected number of arguments. Expected: 02_get_ncRNA_GRanges.R <custom annot priority> <family.txt.gz> <genome.fa> <rfam.gff3.gz> <rRNAs.gff> <tRNAs.gtf.gz> <mapmi.gff3.gz> <miRDeep.bed> [custom annotation]", call.=FALSE)
}

cpriority <- args[1]
if(!(cpriority %in% c("none", "top", "bottom")))
  stop(paste0("Error: Invalid priority of custom annotation: ",
              args[1], ".\n Valid options are: none, top and bottom"))

famFile  <- args[2]
faFile   <- args[3]
rfamFile <-  args[4]
rrnaFile <- args[5]
trnaFile <- args[6]
mapmiFile <- args[7]
mirdeepFile <- args[8]
if(cpriority != "none")
  custmAntFile <- args[9]
outFile  <- "ncRNAs_GR.Rds"
outFileGff <- "ncRNAs.gff3.gz"

famTab <- read.delim(famFile, header=FALSE)
colnames(famTab)[2] <- "id"
colnames(famTab)[4] <- "name"
colnames(famTab)[9] <- "cutoff"
rownames(famTab) <- famTab$id

# Using FaIndex
sInfo <- seqinfo(scanFaIndex(faFile))
genomeLength <- sum(seqlengths(sInfo))

# Function for fixing sInfo
fixGR <- function(gr) {
  seqlevels(gr) <- seqlevels(sInfo)
  seqinfo(gr) <- sInfo
  #mcols(gr) <- NULL
  return(gr)
}


# Load Rfam, rRNA, tRNA, miRNA, piRNA, yRNA predictions
rfamGR <- import(rfamFile); rfamGR <- fixGR(rfamGR)
rrnaGR <- import(rrnaFile); rrnaGR <- fixGR(rrnaGR)
mapmiGR <- import(mapmiFile); mapmiGR <- fixGR(mapmiGR)
if(cpriority != "none")
  cstGR <- import(custmAntFile); cstGR <- fixGR(custmAntFile)

# trnascan has a very relaxed score threshold, so increase
# supposedly default is 40, although EnsemblGenomes say they use 65
trnaGR <- import(trnaFile)
trnaGR <- trnaGR[trnaGR$score > 40]
trnaGR <- fixGR(trnaGR)

# mirdeep2 has a very relaxed score threshold, so increase
mirdeepGR <- import(mirdeepFile)
mirdeepGR <- mirdeepGR[mirdeepGR$score > 10]
mirdeepGR <- fixGR(mirdeepGR)

# Rfam has no threshold, fix?
# Rfam can be divided, at least to complete tRNA, rRNA
# Remove 'older?' rfams not in the file I'm using
rfamGR <- rfamGR[rfamGR$type %in% famTab$id]
# Remove those without score > cutoff
rfamGR <- rfamGR[rfamGR$score > famTab[rfamGR$type,"cutoff"]]
# Assign new general class
rfamGR$class <- "other_ncRNA"
if(any(grepl("_rRNA",rfamGR$type)))
  rfamGR[grepl("_rRNA",rfamGR$type)]$class <- "rRNA"
if(any(grepl("^tRNA",rfamGR$type)))
  rfamGR[grepl("^tRNA",rfamGR$type)]$class <- "tRNA"
if(any(grepl("mir-|MIR|let-7|lsy-6",rfamGR$type)))
  rfamGR[grepl("mir-|MIR|let-7|lsy-6",rfamGR$type)]$class <- "miRNA"
if(any(grepl("U\\d$|SmY",rfamGR$type)))
  rfamGR[grepl("U\\d$|SmY",rfamGR$type)]$class <- "snRNA"
if(any(grepl("SNO|^sn|^sR|^ceN",rfamGR$type)))
  rfamGR[grepl("SNO|^sn|^sR|^ceN",rfamGR$type)]$class <- "snoRNA"


# Combine and reduce
if(cpriority == "none"){
  ncGR <- reduce(c(granges(rfamGR), granges(rrnaGR), granges(trnaGR),
                   granges(mapmiGR), granges(mirdeepGR)))
} else {
  ncGR <- reduce(c(granges(rfamGR), granges(rrnaGR), granges(trnaGR),
                   granges(mapmiGR), granges(mirdeepGR), granges(cstGR)))
}

bases <- sum(width(ncGR))
print(paste("All ncRNA, bases =",bases,"| percent =",round(bases / genomeLength * 100,1)))

# Add simple annotation
ncGR$type <- "other_ncRNA"
if(cpriority == "top")
  ncGR[ncGR %over% mapmiGR]$type <- "custom"
ncGR[ncGR %over% mapmiGR]$type <- "mapmiRNA"
if(any("tRNA" %in% rfamGR$class))
  ncGR[ncGR %over% rfamGR[rfamGR$class == "tRNA"]]$type <- "tRNA"
if(any("rRNA" %in% rfamGR$class))
ncGR[ncGR %over% rfamGR[rfamGR$class == "rRNA"]]$type <- "rRNA"
if(any("snoRNA" %in% rfamGR$class))
ncGR[ncGR %over% rfamGR[rfamGR$class == "snoRNA"]]$type <- "snoRNA"
if(any("snRNA" %in% rfamGR$class))
ncGR[ncGR %over% rfamGR[rfamGR$class == "snRNA"]]$type <- "snRNA"
if(any("miRNA" %in% rfamGR$class))
ncGR[ncGR %over% rfamGR[rfamGR$class == "miRNA"]]$type <- "miRNA"
ncGR[ncGR %over% rrnaGR]$type <- "rRNA"
ncGR[ncGR %over% trnaGR]$type <- "tRNA"
ncGR[ncGR %over% granges(mirdeepGR)]$type <- "miRNA"
if(cpriority == "bottom")
  ncGR[ncGR %over% mapmiGR]$type <- "custom"
print(table(ncGR$type))

# Make AS GR
ncGRAS <- ncGR
strand(ncGRAS) <- ifelse(strand(ncGRAS) == "+","-","+")

# Create ncRNA GR objects for further use
ncList <- list("ncRNA_S"=ncGR, "ncRNA_AS"=ncGRAS)

# Save as a GRanges
saveRDS(ncList, file=outFile)

# Write a GFF3 file, for genome browsers
try(export(ncGR, gzfile(outFileGff), "gff3"))