/*
========================================================================================
 Annotate ncRNAs with sRNA-seq data. Started October 2018.

 #### Authors
 Pablo González <pablo.gonzalez@cinvestav.mx>

----------------------------------------------------------------------------------------
*/

/*
 * SET UP CONFIGURATION VARIABLES
 */

// Pipeline version
version = 0.01

// Configurable variables
params.project = false
params.genome = "Celegans.fa"
params.miRNAs = "mature.fa"
params.dustSrc = "custom_dust_releases.tar.bz2"
params.rfam = "data/dbs/Rfam.cm"
params.tabuSeq = "CTGA"
params.phred = "15/4"
params.dust = 20
params.minLen = 16
params.mismatches = 1
params.ranmax = 50
params.bowtieM = params.ranmax
params.chunkSize = 1000
params.reads = "data/*{1,2}.fastq.gz"
params.outdir = './results'


/*
 * Execution example:
 cd /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/
nohup nextflow -C /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/nf_mazorka.conf run /LUSTRE/usuario/pgonzale/ncrna-nf/scripts/seqBase.nf -resume -with-timeline --project t1 --outdir /LUSTRE/usuario/pgonzale/ncrna-nf/output/minimal/ --reads '/LUSTRE/usuario/pgonzale/ncrna-nf/data/raw/*.fastq.gz' --genome /LUSTRE/usuario/pgonzale/ncrna-nf/data/genome/test.fa --rfam /LUSTRE/usuario/pgonzale/ncrna-nf/data/dbs/Rfam.cm --adapterSeq "TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC" --tabuSeq "GTTCAGAGTTCTACAGTCCGACGATC" --chunkSize 1 --miRNAs "/LUSTRE/usuario/pgonzale/ncrna-nf/data/dbs/mature.fa" --dustSrc "/LUSTRE/usuario/pgonzale/masterMon/output/ncRNA/MapMi/HelperPrograms/custom_dust_releases.tar.bz2" > /LUSTRE/usuario/pgonzale/ncrna-nf/logs/nft1$(date +"%y_%F_%T").log 2>&1 &
 */


// Validate inputs

if ( params.genome ){
    Channel
        .fromPath(params.genome)
        .ifEmpty {  exit 1, "genome file not found: ${params.genome}" }
        .into { genome_sBowtie ; genome_rfam ; genome_trnascan ; genome_rnammer ; genome_repMod ; genome_repMask ; genome_MapMi }
}

if ( params.miRNAs ){
    Channel
        .fromPath(params.miRNAs)
        .ifEmpty {  exit 1, "miRNAs' file not found: ${params.miRNAs}" }
        .into { miRNA_MapMi }
}


if ( params.rfam ){
    Channel
        .fromPath(params. rfam)
        .ifEmpty {  exit 1, " rfam file not found: ${params. rfam}" }
        .into {  rfam_cmsearch }
}

if ( params.dustSrc ){
    Channel
        .fromPath(params.dustSrc)
        .ifEmpty {  exit 1, "Custom dust file not found: ${params.dustSrc}" }
        .into { dustSrc }
}

/*
 * Create a channel for input read files
 */
Channel
    .fromFilePairs( params.reads, size: 1 )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}" }
    .into { reads_reaper  }


if( workflow.profile == 'standard' && !params.project ) exit 1, "No project ID found! Use --project"

// Header log info
log.info "========================================="
log.info "ncRNA-nf v${version}"
log.info "========================================="
log.info "Reads             : ${params.reads}"
log.info "Genome            : ${params.genome}"
log.info "miRNAs            : ${params.miRNAs}"
log.info "3' adapter        : ${params.adapterSeq}"
log.info "Tabu              : ${params.tabuSeq} "
log.info "Quality threshold : ${params.phred}"
log.info "DUST score        : ${params.dust} "
log.info "Min length        : ${params.minLen}"
log.info "Mismatches        : ${params.mismatches}"
log.info "Ranmax            : ${params.ranmax}"
log.info "BowtieM           : ${params.bowtieM}"
log.info "Current home      : $HOME"
log.info "Current user      : $USER"
log.info "Current path      : $PWD"
log.info "Script dir        : $baseDir"
log.info "Working dir       : $workDir"
log.info "Output dir        : ${params.outdir}"
log.info "========================================="


Channel
    .fromPath(params.genome)
    .splitFasta(by: params.chunkSize)
    .set { trnascan_fasta }

/*
 * STEP 1.1 - Rfam
 */
process rfam {
  publishDir "${params.outdir}/Rfam", mode: 'copy'

  input:
  file rfam from rfam_cmsearch
  file fasta from genome_rfam

  output:
  file "genome.rfam" into rfam_out

  script:
  """
  cmpress $rfam
  cmsearch --rfam --cpu ${task.cpus} --tblout genome.rfam $rfam $fasta
  """
}

 /*
  * STEP 1.2 - tRNAscan
 */
process trnascan {
    input:
    file fasta from trnascan_fasta

    output:
    file "trnascan.out" into trnascan_frag

    script:
    """
    tRNAscan-SE -Q -o trnascan.out $fasta
    """
}

trnascan_frag.collectFile( name:'tRNAs.gtf', newLine: true, storeDir: "${params.outdir}/tRNAscan")

/*
 * STEP 1.3 - RNAmmer
 */
process rnammer {
   publishDir "${params.outdir}/RNAmmer", mode: 'copy'

   input:
   file fasta from genome_rnammer

   output:
   file "rRNAs.gff" into rnammer_out

   script:
   """
   rnammer -S euk -m tsu,lsu,ssu -gff rRNAs.gff < $fasta
   """
}

/*
 * STEP 1.4 - MapMi
 */

process mapmi {
   tag "$name"
   publishDir "${params.outdir}/MapMi", mode: 'copy'

   input:
   file miRNAs from miRNA_MapMi
   file fasta from genome_MapMi
   file cdust from dustSrc

   output:
   file "MapmiOut.tab" into mapmiOut

   shell:
   """
   mkdir -p HelperPrograms/
   mkdir -p RawData/Others/
   seqret -sequence $fasta -outseq RawData/Others/test.fa
   tar jxf $cdust -C HelperPrograms/
   make -C HelperPrograms/Custom_dust_releases/dust2/
   ln -s \$PWD/HelperPrograms/Custom_dust_releases/dust2/dust2 .
   ln -s /data/software/bowtie-1.2.2-linux-x86_64/bowtie-build HelperPrograms/
   ln -s /data/software/bowtie-1.2.2-linux-x86_64/bowtie HelperPrograms/

   MapMi-MainPipeline-v159-b32.pl --queryFile $miRNAs --min_score 0 --min_len $params.minLen --bowtie_m $params.bowtieM --outputPrefix MapmiOut
   awk 'BEGIN { RS = ">"; ORS = "\\n"; FS = "\\n"; OFS = "\\t" } ; { print \$1, \$2}' MapmiOut.dust | tail -n +2 > MapmiOut.tab
   """
}

/*
 * STEP 1.5 - RepeatModeler


process repeatmodeler {
   publishDir "${params.outdir}/RepeatModeler", mode: 'copy'

   input:
   set val(name), file(fasta) from genome_repMod

   output:
   file("${name}.fa-families.fa") into modRepeats

   shell:
   """
   source activate py2.7
   BuildDatabase -name helpol2 -engine ncbi $fasta
   RepeatModeler -engine ncbi -pa ${task.cpus} -database helpol2
   """
}

/*
 * STEP 1.5 - RepeatMasker
 */

process repeatmasker {
   publishDir "${params.outdir}/RepeatMasker", mode: 'copy'

   input:
   file fasta from genome_repMask
   file spRepeats from modRepeats

   output:
   file("${name}.fa-families.fa")

   shell:
   """
   source activate py2.7
   cat $spRepeats <(queryRepeatDatabase.pl -species nematoda) > all.repeats
   RepeatMasker -gff -lib all.repeats -pa ${task.cpus} $fasta
   """
}

